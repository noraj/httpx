module HTTPX
  class Request
    include Callbacks

    METHODS: Array[verb]
    USER_AGENT: String

    attr_reader verb: verb
    attr_reader uri: URI::HTTP | URI::HTTPS
    attr_reader headers: Headers
    attr_reader body: Body
    attr_reader state: Symbol
    attr_reader options: Options
    attr_reader response: response?
    attr_reader drain_error: StandardError?

    def initialize: (verb | String, uri, ?options?) -> untyped

    def interests: () -> (:r | :w)

    def merge_headers: (Headers | Hash) -> void

    def scheme: () -> ("http" | "https")

    def response=: (response) -> void

    def path: () -> String

    def authority: () -> String

    def origin: () -> String

    def query: () -> String

    def drain_body: () -> String?

    def inspect: () -> String

    def transition: (Symbol) -> void

    def expects?: () -> boolish

    def trailers: () -> Headers

    def trailers?: () -> boolish

    class Body
      def initialize: (Headers, Options) -> untyped
      def each: () { (String) -> void } -> void
              | () -> Enumerable[String]

      def empty?: () -> bool
      def bytesize: () -> Numeric
      def stream: (Transcoder::_Encoder) -> bodyIO
      def unbounded_body?: () -> bool
      def chunked?: () -> bool
      def chunk!: () -> void
      def inspect: () -> String
    end

    class ProcIO
      include _Writer
      def initialize: (^(_ToS) -> void) -> untyped
    end
  end
end
